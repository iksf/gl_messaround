#version 150
uniform mat4 u_Proj;
uniform mat4 u_View;
in vec3 a_Pos;

void main() {
    gl_Position = u_Proj*u_View*  vec4(a_Pos, 1.0);
}