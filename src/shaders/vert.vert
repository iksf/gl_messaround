#version 150
uniform mat4 u_Proj;
uniform mat4 u_View;
in vec3 a_Pos;
out vec3 v_Pos;

void main() {
    v_Pos = a_Pos;
    gl_Position = u_Proj*u_View*  vec4(a_Pos, 1.0);
}