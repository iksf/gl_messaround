#version 150
in vec3 v_Pos;
out vec4 Target;

void main() {
    Target = vec4(v_Pos, 1.0);
}