use glutin;
use glutin::EventsLoop;
use std::sync::mpsc::Sender;

pub struct InputSystem {
    selection_event: SelectionBoxMouseEvent,
}

impl InputSystem {
    pub fn new() -> InputSystem {
        InputSystem {
            selection_event: SelectionBoxMouseEvent::new(),
        }
    }
    pub fn handle_inputs(&mut self, events_loop: &mut EventsLoop) {
        events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => match event {
                glutin::WindowEvent::MouseInput {
                    button: glutin::MouseButton::Left,
                    state,
                    ..
                } => match state {
                    glutin::ElementState::Pressed => {
                        self.selection_event.on_click();
                    }
                    glutin::ElementState::Released => {
                        self.selection_event.on_release();
                    }
                },
                _ => (),
            },
            _ => (),
        });
    }
}
pub struct SelectionBoxMouseEvent {
    on_click: Box<FnMut()>,
    on_release: Box<FnMut()>,
    on_click_debug: Box<Fn()>,
    on_release_debug: Box<Fn()>,
    clicked: bool,
}
impl SelectionBoxMouseEvent {
    fn new() -> SelectionBoxMouseEvent {
        let on_click = Box::new(|| {});
        let on_release = Box::new(|| {});
        let on_click_debug = Box::new(|| {
            println!("Clicked");
        });
        let on_release_debug = Box::new(|| {
            println!("Released");
        });
        SelectionBoxMouseEvent {
            on_click,
            on_click_debug,
            on_release,
            on_release_debug,
            clicked: false,
        }
    }
}

impl LeftClickable for SelectionBoxMouseEvent {
    fn is_clicked(&self) -> bool {
        self.clicked
    }
    fn is_clicked_mut(&mut self) -> &mut bool {
        &mut self.clicked
    }
    fn get_left_click_function(&mut self) -> &mut FnMut() {
        &mut *self.on_click
    }
    fn get_left_release_function(&mut self) -> &mut FnMut() {
        &mut *self.on_release
    }

    fn get_on_click_debug_function(&self) -> Option<&Fn()> {
        Some(&*self.on_click_debug)
    }

    fn get_on_release_debug_function(&self) -> Option<&Fn()> {
        Some(&*self.on_release_debug)
    }
}

pub trait LeftClickable {
    fn is_clicked(&self) -> bool;
    fn is_clicked_mut(&mut self) -> &mut bool;
    fn get_left_click_function(&mut self) -> &mut FnMut();
    fn get_left_release_function(&mut self) -> &mut FnMut();
    fn get_on_click_debug_function(&self) -> Option<&Fn()> {
        None
    }
    fn get_on_release_debug_function(&self) -> Option<&Fn()> {
        None
    }
    fn set_clicked(&mut self) {
        *self.is_clicked_mut() = true
    }

    fn set_released(&mut self) {
        *self.is_clicked_mut() = false
    }
    fn on_click(&mut self) {
        if !self.is_clicked() {
            self.set_clicked();
            if let Some(f) = self.get_on_click_debug_function() {
                f()
            }
        }
        (self.get_left_click_function())()
    }
    fn on_release(&mut self) {
        if self.is_clicked() {
            self.set_released();
            if let Some(f) = self.get_on_release_debug_function() {
                f()
            }
        }

        (self.get_left_release_function())()
    }
}
