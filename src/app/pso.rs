use app::formats::Sampling;
use app::shaders::Shader;
pub struct PipelineStateObject {
    shader: Shader,
    sampling: Sampling,
}
