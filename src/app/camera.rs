use app::shaders::Shader;
use cgmath;
use cgmath::*;
use gl;
use std::ffi::CString;
pub struct Camera {
    camera_pos: cgmath::Vector3<f32>,
    camera_front: cgmath::Vector3<f32>,
    camera_up: cgmath::Vector3<f32>,
    view: cgmath::Matrix4<f32>,
    perspective: cgmath::Matrix4<f32>,
}

impl Camera {
    pub fn new() -> Camera {
        let camera_pos = cgmath::vec3(0., 0.0, -3.0);
        let camera_front = cgmath::vec3(0.0, 0.0, -1.0);
        let camera_up = cgmath::vec3(0.0, 1.0, 0.0);
        let view: Matrix4<f32> = {
            cgmath::Matrix4::look_at(
                cgmath::Point3::from_vec(camera_pos),
                cgmath::Point3::from_vec(camera_front),
                camera_up,
            )
        };

        let perspective: cgmath::Matrix4<f32> =
            cgmath::perspective(cgmath::Deg(68.), 800. / 500., 1., 5000.);
        Camera {
            camera_front,
            camera_pos,
            camera_up,
            perspective,
            view,
        }
    }

    pub fn update(&mut self) {
        self.view = cgmath::Matrix4::look_at(
            cgmath::Point3::from_vec(self.camera_pos),
            cgmath::Point3::from_vec(self.camera_front),
            self.camera_up,
        )
    }

    pub fn update_camera_view_uniform(&self, shader: &Shader) {
        unsafe {
            gl::UniformMatrix4fv(
                gl::GetUniformLocation(shader.id(), CString::new("u_View").unwrap().as_ptr()),
                1,
                gl::FALSE,
                &self.view.x.x as *const f32,
            );
        }
    }
    pub fn update_both_view_camera_uniforms(&self, shader: &Shader) {
        self.update_camera_projecton_uniform(shader);
        self.update_camera_view_uniform(shader);
    }

    pub fn update_camera_projecton_uniform(&self, shader: &Shader) {
        unsafe {
            gl::UniformMatrix4fv(
                gl::GetUniformLocation(shader.id(), CString::new("u_Proj").unwrap().as_ptr()),
                1,
                gl::FALSE,
                &self.perspective.x.x as *const f32,
            );
        }
    }
}
