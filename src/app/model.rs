use app::shaders::Shader;
use gl;
use std::collections::HashMap;
use std::os::raw::c_void;
use std::path::Path;
use tobj;
#[derive(Clone)]
pub struct Model {
    pub points: Vec<f32>,
    pub indices: Vec<u32>,
}

impl Model {
    pub fn new(points: Vec<f32>, indices: Vec<u32>) -> Model {
        Model { points, indices }
    }

    pub fn from_path(path: &str) -> Model {
        let (ad, _) = tobj::load_obj(&Path::new(&(env!("CARGO_MANIFEST_DIR").to_owned() + path)))
            .expect("No file");
        let vertices = ad.iter().nth(0).expect("Empty file").mesh.positions.clone();
        let indices = ad.iter().nth(0).expect("Empty file").mesh.indices.clone();
        use app::model::Model;
        Model::new(vertices, indices)
    }
    pub fn into_generic_gl_model<S: Into<String> + Clone>(
        self,
        hashmap: &mut HashMap<String, GenericGlModel>,
        name: S,
    ) {
        GenericGlModel::new(self, hashmap, name)
    }
}

#[derive(Clone)]
pub struct GenericGlModel {
    pub vao: u32,
    pub vbos: Vec<u32>,
    pub ebo: u32,
    pub model: Model,
}

pub trait GlModel {
    fn vao(&self) -> u32;
    fn vbos(&self) -> &Vec<u32>;
    fn ebo(&self) -> u32;
    fn vao_mut(&mut self) -> &mut u32;
    fn vbos_mut(&mut self) -> &mut Vec<u32>;
    fn ebo_mut(&mut self) -> &mut u32;
    fn model(&self) -> &Model;
    fn bind(&self) {
        unsafe {
            gl::BindVertexArray(self.vao());
        }
    }
    fn draw_with(&self, shader: &Shader) {
        unsafe {
            self.bind();
            shader.bind();
            gl::DrawElements(
                gl::TRIANGLES,
                self.model().indices.len() as i32,
                gl::UNSIGNED_INT,
                ::std::ptr::null(),
            );
        }
    }
    fn gl_constructor(&mut self) {
        unsafe {
            gl::GenVertexArrays(1, self.vao_mut());
            gl::BindVertexArray(self.vao());
            gl::GenBuffers(1, &mut self.vbos_mut()[0]);
            gl::GenBuffers(1, self.ebo_mut());
            gl::BindBuffer(gl::ARRAY_BUFFER, self.vbos()[0]);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                4 * self.model().points.len() as isize,
                &self.model().points[0] as *const f32 as *const c_void,
                gl::STATIC_DRAW,
            );
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.ebo());

            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                4 * self.model().indices.len() as isize,
                &self.model().indices[0] as *const u32 as *const c_void,
                gl::STATIC_DRAW,
            );

            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 3 * 4, ::std::ptr::null());
            gl::EnableVertexAttribArray(0);
        }
    }
}
impl GlModel for GenericGlModel {
    fn vao(&self) -> u32 {
        self.vao
    }
    fn vbos(&self) -> &Vec<u32> {
        &self.vbos
    }
    fn ebo(&self) -> u32 {
        self.ebo
    }
    fn vao_mut(&mut self) -> &mut u32 {
        &mut self.vao
    }
    fn vbos_mut(&mut self) -> &mut Vec<u32> {
        &mut self.vbos
    }
    fn ebo_mut(&mut self) -> &mut u32 {
        &mut self.ebo
    }
    fn model(&self) -> &Model {
        &self.model
    }
}
impl GenericGlModel {
    pub fn new<S: Into<String> + Clone>(
        model: Model,
        hashmap: &mut HashMap<String, GenericGlModel>,
        name: S,
    ) {
        let mut vao = 0;
        let mut vbos = Vec::new();
        let mut vbo = 0;
        let mut ebo = 0;
        vbos.push(vbo);

        let mut glmodel = GenericGlModel {
            vao,
            ebo,
            vbos,
            model,
        };

        hashmap.insert(name.clone().into(), glmodel);
        hashmap.get_mut(&name.into()).unwrap().gl_constructor();
    }
}
