use gl;
use glutin;
use glutin::EventsLoop;
use glutin::GlContext;
use glutin::GlWindow;
pub struct Window {
    window: GlWindow,
    //events_loop: EventsLoop,
}

impl Window {
    pub fn new() -> (Window, EventsLoop) {
        let window_builder = glutin::WindowBuilder::new()
            .with_dimensions(glutin::dpi::LogicalSize::new(800., 500.))
            .with_title("Hello world");

        let context = glutin::ContextBuilder::new();
        let events_loop = EventsLoop::new();
        let gl_window = glutin::GlWindow::new(window_builder, context, &events_loop).unwrap();
        (Window { window: gl_window }, events_loop)
    }
    pub fn init(&mut self) {
        gl::load_with(|s| self.window.get_proc_address(s) as *const _);
        unsafe {
            self.window.make_current().unwrap();
            gl::ClearColor(1.0, 0.3, 0.3, 1.0);
            gl::Enable(gl::DEPTH_TEST);
            gl::Enable(gl::CULL_FACE);
            gl::CullFace(gl::BACK);
            gl::FrontFace(gl::CCW);
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
        }
    }

    pub fn next_frame(&self) {
        self.window.swap_buffers().unwrap();
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }
    }
}
