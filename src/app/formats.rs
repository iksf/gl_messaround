use gl;

pub enum TextureFormat {
    RGB,
    RGB8,
}

pub enum Sampling {
    Linear,
    Nearest,
}
