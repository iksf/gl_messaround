extern crate cgmath;
extern crate gl;
extern crate glutin;
extern crate tobj;
mod app;
use app::camera::Camera;
use app::inputs::InputSystem;
use app::model::GenericGlModel;
use app::model::GlModel;
use app::model::Model;
use app::window::Window;
use std::collections::HashMap;
use std::thread;
fn main() {
    let (mut window, mut events_loop) = Window::new();
    thread::spawn(move || {
        window.init();
        let shader = shader_program!("Main", "/src/shaders/vert.vert", "/src/shaders/frag.frag");
        let selection_shader = shader_program!(
            "Selection",
            "/src/shaders/selection.vert",
            "/src/shaders/selection.frag"
        );

        let mut model = Model::from_path("/src/models/monkey.obj");

        for i in 0..model.points.len() {
            if i % 3 != 2 {
                model.points[i] /= 2.
            }
        }
        let mut hashmap = HashMap::new();
        let gl_model = GenericGlModel::new(model, &mut hashmap, "Main");
        let selection_model = Model::from_path("/src/models/cube.obj")
            .into_generic_gl_model(&mut hashmap, "Selection");
        let camera = Camera::new();
        shader.bind();
        camera.update_both_view_camera_uniforms(&shader);

        selection_shader.bind();
        camera.update_both_view_camera_uniforms(&selection_shader);
        let mut switch = true;
        loop {
            hashmap.get("Main").unwrap().draw_with(&shader);
            if switch {
                hashmap
                    .get("Selection")
                    .unwrap()
                    .draw_with(&selection_shader);
            } else {
            }
            switch = !switch;

            window.next_frame();
            std::thread::sleep_ms(1000);
        }
    });
    let mut inputs = InputSystem::new();
    loop {
        inputs.handle_inputs(&mut events_loop);
        //std::thread::sleep_ms(1000 / 60);
    }
}
